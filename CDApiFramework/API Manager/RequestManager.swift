//
//  RequestManager.swift
//  CDApiFramework
//
//  Created by Navneet Singh Gill on 1/23/19.
//  Copyright © 2019 Paragon Business Solutions Ltd. All rights reserved.
//

import UIKit

public class RequestManager: NSObject {

    private var isAppOnlineAndNetworkAvailable: Bool {
        return !(CDApiManager.shared.isDemoModeOn()) && isNetworkReachable()
    }
    
    public func registerVendorID(with request: Request, completionHandler: @escaping completionHandler) {
        if isAppOnlineAndNetworkAvailable {
            WebServiceHandler().callApi(with: request) { (success, response) in
                completionHandler(success, response)
            }
        } else {
            completionHandler(false, "Please check your network connection.")
        }
    }
    
    public func doCallAPI(with parameters: [String:Any], apiType : CDApiConstants, completionHandler: @escaping completionHandler) {
        if isAppOnlineAndNetworkAvailable {
            WebServiceHandler().callApi(with: Request(parameters, type: apiType)) { (success, response) in
                completionHandler(success, response)
            }
        } else {
            completionHandler(false, "Please check your network connection.")
        }
    }
    
}
