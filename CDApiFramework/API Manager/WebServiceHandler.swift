//
//  WebServiceHandler.swift
//  CasaDigi
//
//  Created by Namrata Jain on 06/06/18.
//  Copyright © 2018 Paragon Business Solutions Ltd. All rights reserved.
//

import UIKit

public enum RequestType:String {
    case GET = "GET"
    case POST = "POST"
    case PATCH = "PATCH"
    case DELETE = "DELETE"
    case PUT = "PUT"
}

public enum ResponseType : String{
    case Success = "success"
    case Error = "error"
}

public typealias completionHandler = (_ success: Bool, _ : Any?) -> Swift.Void

public class WebServiceHandler: NSObject, URLSessionDelegate {
    /*
     jsonUrlRequest: To send Json object in form of DATA attached in HTTP.body and Content-type in Header.
     */
    public func jsonUrlRequest(urlString:String, requestType: RequestType, serviceType: String, json: [String:Any]?, completionHandler: @escaping completionHandler) {
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        
        if json != nil {
            do{
                if requestType == .POST || requestType == .PUT || requestType == .DELETE  {
                    var requestData = ""
                    
                    request.httpMethod = requestType.rawValue
                    
                    let param = JSONStringify(json as AnyObject)
                    
                    let body = NSData(data: param.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!) as Data
                    request.addValue("application/json",forHTTPHeaderField: "Content-Type")
                    request.httpBody = body
                    self.sendRequest(request) { (success, response) in
                        completionHandler(success, response)
                    }
                }
                    
                else if requestType == .GET
                {
                    var requestData = ""
                    
                    request.httpMethod = requestType.rawValue
                    
                    let param = JSONStringify(json as AnyObject)
                    
                    let body = NSData(data: param.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!) as Data
                    request.addValue("application/json",forHTTPHeaderField: "Content-Type")
                    request.httpBody = body
                    self.sendRequest(request) { (success, response) in
                        completionHandler(success, response)
                    }
                }
            }
            catch _ as NSError {
                
                //If any error occur in api response we pass nil in parameter to handle error response.
                DispatchQueue.main.async {
                    completionHandler(false, ["error" : "nil"])
                }
            }
        }
        else {
            
            print("request \(String(describing: request.url))")
            request.httpMethod = requestType.rawValue
            
            self.sendRequest(request) { (success, response) in
                completionHandler(success, response)
            }
        }
    }
    
    
    public func urlRequest(urlString:String, requestType: RequestType, serviceType: String, json: [String:Any]?, target: AnyObject?, action: Selector) {
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        
        if json != nil {
            do{
                if requestType == .POST || requestType == .PUT  {
                    var requestData = ""
                    for object in json as! [String:String] {
                        if object.value.count > 0 {
                            requestData += object.key + "=" + object.value + "&"
                        }
                    }
                    print("request \(String(describing: request.url)) and \(requestData)")
                    request.httpMethod = requestType.rawValue
                    print("http method \(requestType.rawValue)")
                    request.httpBody = requestData.data(using: String.Encoding.utf8)
                    self.sendRequest(request, target: target, action: action)
                }
                    
                else if requestType == .GET || requestType == .DELETE
                {
                    var requestData = ""
                    for object in json as! [String:String] {
                        if object.value.count > 0 {
                            requestData += object.key + "=" + object.value + "&"
                        }
                    }
                    request.url = URL(string: urlString + "?" + requestData)
                    request.httpMethod = requestType.rawValue
                    print("request \(String(describing: request))")

                    self.sendRequest(request, target: target, action: action)
                }
            }
            catch _ as NSError {
                if (target != nil)
                {
                    //If any error occur in api response we pass nil in parameter to handle error response.
                    target!.performSelector(onMainThread: action, with: ["error" : "nil"], waitUntilDone: false)
                }
            }
        }
        else {

            print("request \(String(describing: request.url))")
            request.httpMethod = requestType.rawValue

            self.sendRequest(request, target: target, action: action)
        }
    }
    
    public func sendRequest(_ urlRequest: URLRequest, target: AnyObject?, action: Selector) {
        if !CDApiManager.shared.isDemoModeOn() {
            if !UIApplication.shared.isNetworkActivityIndicatorVisible
            {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
               
                if data != nil && error == nil {
                    DispatchQueue.main.async {
                        do {
                            let jsonDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String , AnyObject>
                            
                            target?.performSelector(onMainThread: action, with: jsonDictionary, waitUntilDone: false)
                             UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                        catch {
                            if (target != nil)
                            {
                                //If any error occur in api response we pass nil in parameter to handle error response.
                                target!.performSelector(onMainThread: action, with: ["error" : "nil"], waitUntilDone: false)
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                        }
                    }
                } else {
                    target!.performSelector(onMainThread: action, with: ["error" : "nil"], waitUntilDone: false)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    #if DEBUG
                        print("#ERROR", #file, #function, "Line#", #line, ": ", error?.localizedDescription as Any)
                    #endif
                }
                }).resume()
            
        }
    }
    
    public func urlRequest(urlString:String, requestType: RequestType, serviceType: String, json: [String:Any]?, completionHandler: @escaping completionHandler) {
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        
        if json != nil {
            do{
                if requestType == .POST || requestType == .PUT  {
                    var requestData = ""
                    for object in json as! [String:String] {
                        if object.value.count > 0 {
                            requestData += object.key + "=" + object.value + "&"
                        }
                    }
                    request.httpMethod = requestType.rawValue
                    request.httpBody = requestData.data(using: String.Encoding.utf8)
                    self.sendRequest(request) { (success, response) in
                        completionHandler(success, response)
                    }
                }
                    
                else if requestType == .GET || requestType == .DELETE
                {
                    var requestData = ""
                    for object in json as! [String:String] {
                        if object.value.count > 0 {
                            requestData += object.key + "=" + object.value + "&"
                        }
                    }
                    request.url = URL(string: urlString + "?" + requestData)
                    request.httpMethod = requestType.rawValue
                    print("request \(String(describing: request))")
                    
                    self.sendRequest(request) { (success, response) in
                        completionHandler(success, response)
                    }
                }
            }
            catch _ as NSError {
                
                //If any error occur in api response we pass nil in parameter to handle error response.
                DispatchQueue.main.async {
                    completionHandler(false, ["error" : "nil"])
                }
            }
        }
        else {
            
            print("request \(String(describing: request.url))")
            request.httpMethod = requestType.rawValue
            
            self.sendRequest(request) { (success, response) in
                completionHandler(success, response)
            }
        }
    }
    
    public func sendRequest(_ urlRequest: URLRequest, completionHandler: @escaping completionHandler) {
        if !CDApiManager.shared.isDemoModeOn() {
            if !UIApplication.shared.isNetworkActivityIndicatorVisible
            {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
                if data != nil && error == nil {
                    DispatchQueue.main.async {
                        do {
                            let jsonDictionary = try JSONSerialization.jsonObject(with: data!, options: [])
                            DispatchQueue.main.async {
                                completionHandler(true, jsonDictionary as AnyObject)
                            }
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                        catch {
                            //If any error occur in api response we pass nil in parameter to handle error response.
                            DispatchQueue.main.async {
                                completionHandler(false, ["error" : "nil"])
                            }
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        completionHandler(false, response)
                    }
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    #if DEBUG
                    print("#ERROR", #file, #function, "Line#", #line, ": ", error?.localizedDescription as Any)
                    #endif
                }
            }).resume()
            
        }
    }
    
    public func JSONStringify(_ value: AnyObject,prettyPrinted:Bool = false) -> String {
        
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        
        if JSONSerialization.isValidJSONObject(value) {
            
            do{
                let data = try JSONSerialization.data(withJSONObject: value, options: options)
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    return string as String
                }
            }catch {
                //FIXME: HANDLE ERROR APPROPRIATELY.
            }
            
        }
        return ""
        
    }
    
    public func callApi(with request: Request, completionHandler: @escaping completionHandler) {
        if let urlString = request.url, let url = URL(string: urlString) {
            
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = request.requestType.rawValue
            
            if let parameterString = request.parameterString, parameterString.count > 0 {
                urlRequest.httpBody = parameterString.data(using: String.Encoding.utf8)
                
                self.sendRequest(urlRequest) { (success, response) in
                    completionHandler(success, response)
                }
                
            } else if let parameters = request.parameters, parameters.keys.count > 0 {
                
                let param = JSONStringify(parameters as AnyObject)
                let body = NSData(data: param.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!) as Data
                
                urlRequest.addValue("application/json",forHTTPHeaderField: "Content-Type")
                urlRequest.httpBody = body
                
                self.sendRequest(urlRequest) { (success, response) in
                    completionHandler(success, response)
                }
            } else {
                
                print("request \(String(describing: request.url))")
                
                self.sendRequest(urlRequest) { (success, response) in
                    completionHandler(success, response)
                }
            }
        }
    }
}
