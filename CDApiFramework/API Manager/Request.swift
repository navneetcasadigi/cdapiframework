//
//  Request.swift
//  CDApiFramework
//
//  Created by Navneet Singh Gill on 1/23/19.
//  Copyright © 2019 Paragon Business Solutions Ltd. All rights reserved.
//

import UIKit

public class Request: NSObject {

    public var url: String?
    public var parameters: [String:Any]?
    public var parameterString: String?
    public var requestType: RequestType = .GET
    
    
    init(_ parameters: [String: Any], type : CDApiConstants) {
        switch type {
        case .TidalLogin:
            self.url = CDApiBaseURL.kTidalBaseUrl
            self.requestType = .POST
            break
        default:
            break
        }
        self.parameters = parameters
    }
}
