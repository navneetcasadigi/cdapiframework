//
//  APIHandler.swift
//  CasaDigi
//
//  Created by Vaibhav Jain on 04/05/18.
//  Copyright © 2018 Paragon Business Solutions Ltd. All rights reserved.
//

import UIKit

extension APIHandler {
    //MARK: - Service Methods
    
    //Delete Songs From Playlist
    public class func deleteMusicFromPlaylist(param:[String:Any], target: AnyObject?, action: Selector){
        connectServerWithParameters(parameters: param , serviceType: DELETESONGSFROMPLAYLIST, target: target, action: action)
    }
    //Channel Fav
    public class func televisonChannelFav(param:[String:Any], target: AnyObject?, action: Selector){
        connectServerWithParameters(parameters: param , serviceType: TELEVISONCHANNELFAV, target: target, action: action)
    }
   
}

public class APIHandler: NSObject {
    
    static public var MusicWebserviceURL: String {
        return "http://\(CDApiManager.shared.entertainmentServerAddress())/play_list/playlist_new.php?"
    }
    static public var Television: String {
        return "http://\(CDApiManager.shared.entertainmentServerAddress())/mymovie/index.php/favourite/fav"
    }
    
    // MARK: - API
    static public let  DELETESONGSFROMPLAYLIST =  "delete_song"
    static public let  TELEVISONCHANNELFAV =  "Channel_fav"
    
    
    
    // MARK: - General Request Method
    public class func connectServerWithParameters(parameters: [String: Any],urlString:String = "",serviceType: String, target: AnyObject?, action: Selector) {
        if isNetworkReachable() {
            switch serviceType
            {
            case DELETESONGSFROMPLAYLIST:
                APIHandler.createRequestAndSend(urlString: MusicWebserviceURL, requestType: RequestType.POST, serviceType: serviceType, json: parameters, target: target, action: action)
                
            case TELEVISONCHANNELFAV:
                APIHandler.createRequestAndSend(urlString: TELEVISONCHANNELFAV, requestType: RequestType.POST, serviceType: serviceType, json: parameters, target: target, action: action)
            default:
                break
            }
        }else{
            if (target != nil)
            {
                //If any Network not availble.
                target!.performSelector(onMainThread: action, with: ["error" : "Network"], waitUntilDone: false)
            }
        }
    }
    
    public class func createRequestAndSend(urlString:String, requestType: RequestType, serviceType: String, json: [String:Any]?, target: AnyObject?, action: Selector) {
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        if requestType == .POST {
            request.httpMethod = RequestType.POST.rawValue
        }
        
        if json != nil {
            do{
                if requestType == .POST {
                    var requestData = ""
                    for object in json as! [String:String] {
                        if object.value.count > 0 {
                            requestData += object.key + "=" + object.value + "&"
                        }
                    }
                    request.httpBody = requestData.data(using: String.Encoding.utf8)
                }
                else if requestType == .GET
                {
                    var requestData = ""
                    for object in json as! [String:String] {
                        if object.value.count > 0 {
                            requestData += object.key + "=" + object.value + "&"
                        }
                    }
                    request.url = URL(string: urlString + "?&" + requestData)
                }
                
                APIHandler.sendRequest(request: request, serviceName: nil, serviceType: serviceType, target: target, action: action)
            }
            catch _ as NSError {
                if (target != nil)
                {
                    //If any error occur in api response we pass nil in parameter to handle error response.
                    target!.performSelector(onMainThread: action, with: ["error" : "nil"], waitUntilDone: false)
                }
            }
        }
        else
        {
            if (target != nil)
            {
//                let errorEntity = ErrorEntity(withMessage:TSLocalizedString(MessageKeys.COMMON_ERROR))
//                target!.performSelector(onMainThread: action, with: errorEntity, waitUntilDone: false)
            }
        }
    }
    
    
    // MARK: - COMMON
    public class func sendRequest(request: URLRequest, serviceName: String?, serviceType: String, target: AnyObject?, action: Selector){
        
        let session: URLSession = URLSession.shared
        
        if !UIApplication.shared.isNetworkActivityIndicatorVisible
        {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        let task: URLSessionDataTask = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            if data != nil
            {
                let string = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
               
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves)
                    if let responseResult = result as? [String:AnyObject] {
                        target!.performSelector(onMainThread: action, with: responseResult, waitUntilDone: false)
                    }
                    else
                    {
                    }
                }
                catch _ as NSError {
                    if (target != nil)
                    {
                        //If any error occur in api response we pass nil in parameter to handle error response.
                        target!.performSelector(onMainThread: action, with: ["error" : "nil"], waitUntilDone: false)
                    }
                }
            }
            else
            {
                if (target != nil)
                {
                     //If any error occur in api response we pass nil in parameter to handle error response.
                    target!.performSelector(onMainThread: action, with: ["error" : "nil"], waitUntilDone: false)
                }
            }
        })
        task.resume()
    }
}
