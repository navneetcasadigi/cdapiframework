//
//  CDApiConstants.swift
//  CDApiFramework
//
//  Created by Navneet Singh Gill on 1/22/19.
//  Copyright © 2019 Paragon Business Solutions Ltd. All rights reserved.
//

import UIKit

public struct CDApiBaseURL{
     static public let  kTidalBaseUrl =  "http://10.13.36.247/mymovie/tidal/index"
}

public func isNetworkReachable() -> Bool {
    return (Reachability()!.currentReachabilityStatus != Reachability.NetworkStatus.notReachable)
}

public enum CDApiConstants {
    case TidalLogin
    case TidalPlaylist
}

public struct ApiUrl {
     static let kTidalLogin = "userLogin"
}
