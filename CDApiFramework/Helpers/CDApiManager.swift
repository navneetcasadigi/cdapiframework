//
//  CDApiManager.swift
//  CDApiFramework
//
//  Created by Navneet Singh Gill on 1/22/19.
//  Copyright © 2019 Paragon Business Solutions Ltd. All rights reserved.
//

import UIKit

public protocol CDApiManagerDatasource {
    func entertainmentServerAddress() -> String
    func isDemoModeOn() -> Bool
}

public class CDApiManager: NSObject {

    public static let shared: CDApiManager = CDApiManager()
    
    public var datasource: CDApiManagerDatasource?
    
    public func entertainmentServerAddress() -> String {
        return datasource?.entertainmentServerAddress() ?? ""
    }
    
    public func isDemoModeOn() -> Bool {
        return datasource?.isDemoModeOn() ?? true
    }
    
}
