//
//  CDApiFramework.h
//  CDApiFramework
//
//  Created by Navneet Singh Gill on 1/22/19.
//  Copyright © 2019 Paragon Business Solutions Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CDApiFramework.
FOUNDATION_EXPORT double CDApiFrameworkVersionNumber;

//! Project version string for CDApiFramework.
FOUNDATION_EXPORT const unsigned char CDApiFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CDApiFramework/PublicHeader.h>


